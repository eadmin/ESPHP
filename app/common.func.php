<?php 
use esclass\database;
function hasfocus($touid,$uid){

	$fs=DB('zan')->where(['uid'=>$touid,'sid'=>$uid,'type'=>0])->count();

	$gz=DB('zan')->where(['uid'=>$uid,'sid'=>$touid,'type'=>0])->count();

	if($gz>0){
		//这个是我关注的人
		if($fs>0){
			//这个是我的粉丝
			$hasfocus=3;//好友
		}else{
			$hasfocus=2;//关注

		}
	}else{
		if($fs>0){
			//这个是我的粉丝
			$hasfocus=1;//粉丝
		}else{
			//没有任何关系

		}





	}
	return $hasfocus;
}
function utf8RawUrlDecode ($source) {
	$decodedStr = "";
	$pos = 0;
	$len = strlen ($source);
	while ($pos < $len) {
		$charAt = substr ($source, $pos, 1);
		if ($charAt == '%') {
			$pos++;
			$charAt = substr ($source, $pos, 1);
			if ($charAt == 'u') {
				// we got a unicode character
				$pos++;
				$unicodeHexVal = substr ($source, $pos, 4);
				$unicode = hexdec ($unicodeHexVal);
				$entity = "&#". $unicode . ';';
				$decodedStr .= utf8_encode ($entity);
				$pos += 4;
			}
			else {
				// we have an escaped ascii character
				$hexVal = substr ($source, $pos, 2);
				$decodedStr .= chr (hexdec ($hexVal));
				$pos += 2;
			}
		} else {
			$decodedStr .= $charAt;
			$pos++;
		}
	}
	return $decodedStr;
}
// 获取访问token
function get_access_token($key='')
{

	if($key==''){
		return sha1('EasySNS' . date("Ymd") . API_KEY);
	}else{
		return sha1('EasySNS' . date("Ymd") . $key);
	}

}
function getartcatenamebyid($id){

	$info = DB('articlecate')->where(['id' =>$id])->value('name');

	return $info;


}

//获取图片
function getcontentimage($content){
	preg_match_all("/\<img.*?src\=\"(.*?)\"[^>]*>/i", $content, $images);
	if($images){
		return $images;
	}else{
		return '';
	}

}
function usercz($uid,$did,$type=2,$cid=1){

	$data['uid']=$uid;
	$data['did']=$did;
	$data['create_time']=time();
	$data['type']=$type;
	$data['cid']=$cid;

	if($cid==3&&$type==2&&$uid==$did){
		return;
	}else{
		database::getInstance()->table('usercz')->insert($data);
	}



}

/**
 * 获取图片url
 */
function get_picture_url($id = 0)
{

	$info = database::getInstance()->table('Picture')->where(['id' => $id])->field('path,url')->getRow();

	if (!empty($info['url']))  : return $info['url'];  endif;



	if (!empty($info['path'])) : return WEB_PATH_PICTURE.$info['path'];  endif;

	return '__PUBLIC__/images/onimg.png';
}


function getusernamebyid($uid){
	if($uid==0){
		return '所有人';
	}else{
		$children = DB('user')->where(['id' =>$uid])->getRow();
		if(empty($children)){

			$children = DB('admin_user')->where(['id' =>$uid])->getRow();
			return $children['nickname'];
		}else{
			return $children['nickname'];
		}

	}



}
/*
 * 来判断导航链接内部外部从而生成新链接
 *
 *
 */
function getnavlink($link, $sid) {
	if ($sid == 1) {

		$arr = explode ( ',', $link );

		$url = $arr [0];

		array_shift ( $arr );
		if (empty ( $arr )) {

			$link = routerurl ( $url );
		} else {
			$m = 1;
			$queue = array ();
			foreach ( $arr as $k => $v ) {

				if ($m == 1) {
					$n = $v;
					$m = 2;
				} else {
					$b = $v;
					$queue [$n] = $b;
					$m = 1;
				}
			}
			if (empty ( $queue )) {
				$link = routerurl ( $url );
			} else {
				$link = routerurl ( $url, $queue );
			}
		}
	}

	return $link;
}
function routerurl($url, $arr = array()) {
	if (empty ( $arr )) {
		$str = es_url ( $url );
	} else {
		$str = es_url ( $url, $arr );
	}

	$str = str_replace ( 'admin.php', 'index.php', $str );

	return $str;
}

function getnavactive($link){


	//"http://".$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'];
	$mm= strtolower(trim(substr($link,0,strrpos($link, '.'))));
	$local=$_SERVER['REQUEST_URI'];

	if($local==getbaseurl()){
		$local=es_url('index/index');
	}

	if(strpos($local,$mm)===false){

		return false;

	}else{
		return true;
	}



}
function getheadurl($head){
	if(preg_match("/^(http:\/\/|https:\/\/).*$/",$head)){
		return $head;
	}else{
		return detect_site_url().$head;
	}
}
function getheadurlbyid($uid){

	$children = database::getInstance()->table('user')->where(['id' =>$uid])->getRow();

	if(preg_match("/^(http:\/\/|https:\/\/).*$/",$children['userhead'])){
		return $head;
	}else{
		return detect_site_url().$children['userhead'];
	}
}
function getusergrade($gradeid,$uid=0){



	if($gradeid==-1){
		$name='认证会员';
	}else{

		if($uid==0){
			$name=DB('usergrade')->where(['id'=>$gradeid])->value('name');

			if(empty($name)){
				$name='普通会员';
			}



		}else{

			$info=DB('user')->where(['id'=>$uid])->getRow();

			$map['score']=array('elt',$info['expoint1']);

			$res=DB('usergrade')->where($map)->order('score desc')->limit(1)->value('id');


			if(!empty($res)&&$res!=$info['grades']){
				
				$data['grades'] = $res;
				DB('user')->where(['id'=>$uid])->update($data);
					
			}
			$name=DB('usergrade')->where(['id'=>$res])->value('name');
			if(empty($name)){
				$name='普通会员';
			}



		}
	}




	return $name;
}

function asyn_sendmail($data)
{
	$domain=$_SERVER['HTTP_HOST'];

	
	$url=getweburl().es_url('Index/send_mail');

	

    http_curl($url,$data,'POST');



}
/**
 * 发送邮件
 */
function send_email($address, $title, $message)
{
	/*
	 * 邮件发送类
	 * 支持发送纯文本邮件和HTML格式的邮件，可以多收件人，多抄送，多秘密抄送，带附件(单个或多个附件),支持到服务器的ssl连接
	 * 需要的php扩展：sockets、Fileinfo和openssl。
	 * 编码格式是UTF-8，传输编码格式是base64
	 * @example
	 *  */
	$mail = new \extend\sendmail();

	$mail->setServer(webconfig('mailserver'), webconfig('mailusername'), webconfig('mailpassword'), webconfig('mailport'), true); //设置smtp服务器，到服务器的SSL连接
	$mail->setFrom(webconfig('mailusername')); //设置发件人
	$mail->setFromname(webconfig('mailname')); //设置发件人
	$mail->setReceiver($address); //设置收件人，多个收件人，调用多次
	//	 $mail->setCc("XXXX"); //设置抄送，多个抄送，调用多次
	//	 $mail->setBcc("XXXXX"); //设置秘密抄送，多个秘密抄送，调用多次
	//	 $mail->addAttachment( array("XXXX","xxxxx") ); //添加附件，多个附件，可调用多次，第一个文件名是 程序要去抓的文件名，第二个文件名是显示在邮件中的文件名。
	$mail->setMail($title, html_entity_decode($message)); //设置邮件主题、内容



	$mail->sendMail(); //发送



	return $mail->error();
}