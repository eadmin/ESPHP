<?php
namespace app\common\controller;

class HomeBase extends ControllerBase
{
	
	
    public function _initialize()
    {

    	parent::_initialize();
    	
    	$this->assign('actionname',strtolower(CONTROLLER_NAME.'/'.ACTION_NAME));
       
       if(!empty(session('member_info'))){
       	 
       	$userinfo= self::$datalogic->setname('user')->getDataInfo(['id'=>session('member_info')['id']]);
       	
       	$this->assign('userinfo',$userinfo);
       	 
       }
       $uid=is_login();
       if($uid>0){

       }
       $this->assign('nowuid',$uid);
     
       $this->getSystem();//获得全站配置信息
       $this->getNav();//获取前台导航
       $this->autologin();

       
       $pointarr=parse_config_attr(webconfig('scoretype_list'));
       //获得升级积分
       $this->assign('gpointname',$pointarr['expoint1']);
       //获得下载上传的积分名称
       
       $this->assign('pointname',$pointarr['point']); 
       
       
    }
    public function autologin(){
    	
    	if(!is_login()){
    		
    		$user = unserialize(decrypt(cookie('sys_key')));
    		if ((empty($user['userinfo']))){
    			 
    		}else {
    			
    			self::$datalogic->setname('user')->setDataValue(['id' => $user['userinfo']['id']], 'last_login_time', TIME_NOW);
    			
    			 
    			$auth = ['member_id' => $user['userinfo']['id'], 'last_login_time' => TIME_NOW];
    			$cook=array('id'=>$user['userinfo']['id'], 'userinfo'=>$user['userinfo'],'auth'=>$auth);
    			systemSetKey($cook);
    			 
    			
    			session('member_info', $user['userinfo']);
    			session('member_auth', $auth);
    			session('member_auth_sign', data_auth_sign($auth));
    			 
    			 
    		}
    		
    		
    	}
    	
    	
    }
  
    /**
     * 获取站点信息
     */
    public function getSystem()
    {

    }

    /**
     * 获取前端导航列表
     */
    public function getNav()
    {
        if (cache('nav')) {
            $nav = cache('nav');
          
        } else {
            
            $nav =   self::$datalogic->setname('nav')->getDataList(['status' => 1], true, 'sort asc',false);
           
            if (!empty($nav)) {
                cache('nav', $nav);
            }
        }

        $this->assign('nav', $nav);
       
    }


}