<?php
// +----------------------------------------------------------------------
// | Author: Zaker <49007623@qq.com>
// +----------------------------------------------------------------------

//配置文件

return [
		'view_suffix' => 'html',
		'view_depr'    => '/',
    /* 模板常量替换配置 */
		'view_replace_str'  =>  [
				'__ROOT__' => WEB_URL,
				'__INDEX__' => WEB_URL . '/index.php',
				'__UPLOAD__' => WEB_URL . '/uploads',
				'__PUBLIC__' =>WEB_URL. '/public',
					
		],
    

];
